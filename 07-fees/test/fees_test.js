const {expect, solc, ganacheWeb3, logAccounts} = require('chain-dsl/test/helpers')
const {call} = require('chain-dsl')
const deployer = require('../lib/deployer')

describe('Deployment', () => {
    let web3, snaps, accounts, DEPLOYER, OPERATOR, token

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR
        ] = accounts = await web3.eth.getAccounts()

        ;({token} = await deployer.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it('Contract system is deployed', async () => {
        const sym = web3.utils.hexToUtf8((await call(token, 'symbol')))
        expect(sym).equal('TOKUSD')
    })
})

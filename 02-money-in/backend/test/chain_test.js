const {
    expect,
    toBN,
    solcJSON,
    ganacheWeb3,
} = require('02-money-in-chain/test/helpers')
const solcInput = require('02-money-in-chain/solc-input.json')
const deployer = require('02-money-in-chain/lib/deployer')

describe('Chain', function () {
    this.slow(500)

    let web3, accounts, snaps, gate,
        DEPLOYER,
        OPERATOR,
        CUSTOMER

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({gate} = await deployer.base(web3, solcJSON(solcInput), DEPLOYER, OPERATOR))
    })

    beforeEach(async () => {
        snaps.push(await web3.evm.snapshot())
    })

    afterEach(async () => {
        await web3.evm.revert(snaps.pop())
    })

    it('is integrated', async () => {
        await gate.methods
            ['mint(address,uint256)'](CUSTOMER, toBN(10))
            .send({from: OPERATOR})

        await gate.methods
            ['approve(address,uint256)'](OPERATOR, toBN(5))
            .send({from: CUSTOMER})

        await gate.methods
            ['burn(address,uint256)'](CUSTOMER, toBN(3))
            .send({from: OPERATOR})

        expect((await gate.getPastEvents('allEvents', {fromBlock: 0}))
            .map(({event, returnValues}) => {
                return {event, returnValues}
            }))
            .containSubset([{event: 'Burn'}])
    })
})

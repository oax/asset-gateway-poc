const distillEvent = ({event, returnValues}) => {
    const essence = Object.assign({}, returnValues)
    essence.NAME = event
    // Clean up positional arguments for easier debugging
    delete essence[0]
    delete essence[1]
    delete essence[2]
    delete essence[3]
    return essence
}

module.exports = {
    distillEvent
}

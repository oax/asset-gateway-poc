const L = console.log

class App {
    get gate() {
        return this._gate
    }

    constructor(Web3, currentProvider) {
        L('Constructing app...')
        this.web3 = new Web3(currentProvider)
        L('Web3 version:', this.web3.version)
        this.loadContracts()
    }

    async fetchJSON(url) {
        return fetch(url, {headers: new Headers({'Content-Type': 'application/json'})})
    }

    async loadContract(interfaceJsonUrl) {
        const {address, jsonInterface} = await (await this.fetchJSON(interfaceJsonUrl)).json()
        return new this.web3.eth.Contract(jsonInterface, address)
    }

    async loadContracts() {
        this._gate = await this.loadContract('Gate.json')
        this.logSymbol()
    }

    async logSymbol() {
        const sym = this.web3.utils.hexToUtf8(await this.gate.methods.symbol().call())
        L('Token symbol:', sym)
        return sym
    }
}

# Frontend

An Angular 2 frontend for both a RESTful API and an Ethereum network (through Metamask).

# Development

```bash
yarn install
yarn start
open http://localhost:4200/
```

import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  // Url of the Identity Provider
  loginUrl: 'http://localhost:5000/api/v1/users/authorize',

  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin + '/en-US/gateway/dashboard',

  // The SPA's id. The SPA is registered with this id at the auth-server
  clientId: 'client_id',

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid profile email voucher',

  oidc: false
}

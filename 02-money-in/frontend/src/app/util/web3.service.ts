import {Injectable} from '@angular/core';
import Web3 from 'web3';
import {default as TruffleContract} from 'truffle-contract';
import Gate from '../../../../chain/out/Gate.json';
import {Subject,Observable} from 'rxjs/Rx';

declare let window: any;

@Injectable()
export class Web3Service {
  private _name = 'Web3.Service';
  private web3: Web3;
  private contract;
  private deployed;

  public currentAccount;


  private accounts: string[];
  public accountsObservable = new Subject<string[]>();

  //contract loaded or not
  public ready = false;

  //token list
  public tokenLists = [
    {img: '', name: 'AcmeUSD', amount: 1000},
    {img: '', name: 'Acme HKD', amount: 1000},
  ];


  constructor() {
    this.bootstrapWeb3();
  }

  public async bootstrapWeb3() {

    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      this.web3 = new Web3(window.web3.currentProvider);
    } else {
      console.log('No web3? You should consider trying MetaMask!');

      // Hack to provide backwards compatibility for Truffle, which uses web3js 0.20.x
      Web3.providers.HttpProvider.prototype.sendAsync = Web3.providers.HttpProvider.prototype.send;
      // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
      this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
    }

    await this.initContract();

    //updating account every 1 sec
    Observable
      .interval(100)
      .flatMap(i=> Observable.bindNodeCallback(this.web3.eth.getAccounts)())
      .subscribe(
        (accounts:string[])=>{
          if (accounts.length === 0) {
            console.error(`${this._name}: Couldn't get any accounts!`)
            return;
          }

          if (!this.accounts || this.accounts.length !== accounts.length || this.accounts[0] !== accounts[0]) {
            console.log('Observed new accounts');
            this.accountsObservable.next(accounts);
            this.accounts = accounts;

          //Todo this is for testing, remove it
            this.currentAccount = accounts[0];
            console.log(`Set current account: ${this.currentAccount}`)

          }

        },
        (error)=>console.error(`${this._name}: There is an error updating accounts. ${error}`)
      );
  }

  async initContract(){

    const {address, jsonInterface} =  Gate;
    this.contract = new this.web3.eth.Contract(jsonInterface, address);
    this.ready = true;
  }

  /**
   *@param account set by the customer
   */
  public setCurrentAccount(account){
    this.currentAccount = account;
  }

  /**
   * Customer submit deposit request
   */
  async deposit(amount:number){
    const tx = await this.contract.methods.deposit(amount).send({from: this.currentAccount});
  }


  /**
   * Customer submit withdraw request
   */
  async withdraw(amount: number) {
    const tx = await this.contract.methods.withdraw(amount).send({from: this.currentAccount});
  }



  /**
   * Customer transfer tokens
   */

  async transfer(to:string,amount:number){

    const tx = await this.contract.methods.transfer(to,amount).send({from: this.currentAccount});
  }


  /**
   * Operator mint token for a customer
   */

  async mint(address:string,amount:number){
    const tx = await this.contract.methods.mintFor(address,amount).send({from:this.currentAccount});
    console.log(tx)
  }


  /**
   * Operator burns token
   */

  async burn(address: string, amount: number) {
    const tx = await this.contract.methods.burnFrom(address, amount).send({from: this.currentAccount});
    console.log(tx)
  }


  /**
   * Check balance
   * @param {string} address
   * @returns {Promise<any>}
   */
  async balanceOf(address: string) {
    return await this.contract.methods.balanceOf(address).call();
  }
}

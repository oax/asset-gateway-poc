
export class User{
  id: string;
  name: string;

  constructor(id ?: string, name ?: string){
    this.load(id, name);
  }

  save(): void{
    localStorage.setItem('user', JSON.stringify(this));
  }

  /**
   * load user data from LocalStorage user key
   * @return void
   */
  loadForLS(): void{
    let userStr = localStorage.getItem('user');
    if(userStr){
      this.load(JSON.parse(userStr));
    }
  }

  /**
   * load data
   * @param  {string} id   [description]
   * @param  {string} name [description]
   * @return {void}      [description]
   */
  load(id ?: string, name ?: string){
    id ? this.id = id : null;
    name ? this.name = name : null;
  }


}

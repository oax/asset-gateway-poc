import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import { Location } from '@angular/common';
import {Web3Service} from "../../util/web3.service";
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {accountConfig} from '../../config/account.config';

@Component({
  selector: 'app-gateway-submit',
  templateUrl: './gateway-submit.component.html',
  styleUrls: ['./gateway-submit.component.scss']
})
export class GatewaySubmitComponent implements OnInit {

  layouts = [
    {text: 'One', cols: 4, rows: 4, color: 'lightblue'},
    {text: 'Two', cols: 12, rows: 12, color: 'lightgreen'},
    {text: 'Three', cols: 4, rows: 8, color: 'lightpink'},
  ];

  tokenLists = [
    {img: '', name: 'AcmeUSD', amount: 1000},
    {img: '', name: 'Acme HKD', amount: 1000},
  ]


  model = {
    amount: 0,
    ethereumAddress: '0x627306090abab3a6e1400e9345bc60c78a8bef57'
  };

  config = accountConfig;
  constructor(
    private authService: AuthService,
    private location: Location,
    private _web3Service: Web3Service,
    private _router: Router,
    public snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }


  async submit() {

    if (this.model.amount <= 0) {
      this.snackBar.open("Amount is negative", "Okay", {
        duration: 2000,
      });
      return;
    }
    let redirect = "/en-US/gateway/dashboard/deposit/transfer";
    await this._web3Service.deposit(this.model.amount);
    this._router.navigate([redirect]);

  }

  goBack(): void {
    this.location.back();
  }
}

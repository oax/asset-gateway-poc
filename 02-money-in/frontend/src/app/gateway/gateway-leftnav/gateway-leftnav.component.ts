import { Component, OnInit, AfterViewChecked } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Web3Service} from "../../util/web3.service";
import {async} from "q";
import {Subject,Observable} from 'rxjs/Rx'
@Component({
  selector: 'app-gateway-leftnav',
  templateUrl: './gateway-leftnav.component.html',
  styleUrls: ['./gateway-leftnav.component.scss']
})
export class GatewayLeftnavComponent implements OnInit{

  layouts = [
    {text: 'One', cols: 4, rows: 4, color: 'lightblue'},
    {text: 'Two', cols: 12, rows: 12, color: 'lightgreen'},
    {text: 'Three', cols: 4, rows: 8, color: 'lightpink'},
  ];

  tokenLists:any;

  records = [
    {
      no: 'xxxxxxxxx01',
      date: '2017-11-09 10:23:45',
      status: 'SUBMITED',
      currency: 'USD',
      token: 'USD',
      amount: 1000,
      code: 'X50Y930D',
      bank: 'BOCHK xxxxxxxxxx'
    },{
      no: 'xxxxxxxxx02',
      date: '2017-11-10 15:53:45',
      status: 'MINT',
      currency: 'USD',
      token: 'USD',
      amount: 2000,
      code: 'X5A8Y930D',
      bank: 'BOCHK xxxxxxxxxx'
    }

  ];

  selectIndex: string;

  radioValue: string;


  currentAccount: string;
  currentUser:any;


  public usdBalance = 0.0;


  constructor(private authService: AuthService, public web3Service: Web3Service ) {


  }

  async ngOnInit() {

    //monitor if there are accounts available
    this.web3Service.accountsObservable.subscribe(async (accounts) => {
      this.currentAccount = accounts[0];
      this.tokenLists = this.web3Service.tokenLists;
      this.usdBalance = await this.web3Service.balanceOf(this.currentAccount)
    });



    this.currentAccount = this.web3Service.currentAccount;
    this.usdBalance = await this.web3Service.balanceOf(this.web3Service.currentAccount)
  }

  recordClick(iIndex){
    this.selectIndex = iIndex;
    this.radioValue = 'mat-radio-' + iIndex;
  }


  toggle1 = false;
  gotoggle1(): void {
    this.toggle1 = !this.toggle1;
  }
  toggle2 = false;
  gotoggle2(): void {
    this.toggle2 = !this.toggle2;
  }
  toggle3 = false;
  gotoggle3(): void {
    this.toggle3 = !this.toggle3;
  }
  toggle4 = false;
  gotoggle4(): void {
    this.toggle4 = !this.toggle4;
  }
}




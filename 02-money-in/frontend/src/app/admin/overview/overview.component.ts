import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {DepositRequest, DepositService} from "../../services/deposit.service";
import {Observable} from 'rxjs/Observable';
import {MatTableModule} from '@angular/material/table';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})

export class OverviewComponent implements OnInit {

  actions= [{
    name: 'Mint',
  },{
    name: 'Redemption',
  },{
    name: 'Amount',
  }];

  public depositRequests: DepositRequest[] = [];


  //table
  displayedColumns = ['date', 'id',  'userId', 'currency','amount','state','action'];
  dataSource = new MatTableDataSource();

  constructor(private _depositService: DepositService) {
  }

  ngOnInit() {
    this._depositService.allRequest().subscribe(x => {
      this.depositRequests = x;
      this.dataSource.data = this.depositRequests;
    })
  }

}

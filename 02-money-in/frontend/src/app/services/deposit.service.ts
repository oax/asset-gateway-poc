import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import * as _ from 'lodash';
import {of} from 'rxjs/observable/of';
import {catchError, tap} from 'rxjs/operators';
import * as uuid from 'uuid'


export interface DepositRequest {

  id: number;
  ccy: string;
  amount: number;
  userId: string,
  state: string
  address: string,
  timestamp:number,

  //transfer id to represent current transaction
  transferId:string,

  //banking transfer code
  transferCode:string
}


@Injectable()
export class DepositService {

  public requests: DepositRequest[];

  constructor(
    private _http: HttpClient
  ) { }

  allRequest(): Observable <DepositRequest[]>{

    return  this._http.get('http://localhost:5000/api/v1/deposit-requests')

      .map((res: any) => {

        return _.map(res.data, (value, key) =>{

          return {
            id: key,
            ccy: 'USD',
            amount: value.amount,
            userId: value.ethereumAddress,
            state: value.status,
            address: value.ethereumAddress,
            timestamp: value.timestamp,

            transferId: uuid.v4(),
            transferCode: uuid.v4(),


          }
        })

      })
      .pipe(
        tap(x=>this.requests = x),
        catchError(this.handleError('getHeroes', []))
      );
  }


  requestById(id: Observable<string>):Observable<DepositRequest>{

    return id.flatMap(id => {
      let r =  _.find(this.requests,x=>`${x.id}`===id)
      return of(r)
    })
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}

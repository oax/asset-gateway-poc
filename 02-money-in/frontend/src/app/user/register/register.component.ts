import { Component, OnInit } from '@angular/core';
import {Web3Service} from '../../util/web3.service';
import {HttpClient} from '@angular/common/http';
import {Router, NavigationExtras} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  model = {
    username: '',
    password: '',
    ethereumAddress: ''
  };

  constructor(private web3Service: Web3Service, private _http: HttpClient, private _router: Router, private _translateService: TranslateService) {

    //monitor if there are accounts available
    this.web3Service.accountsObservable.subscribe((accounts) => {
      this.model.ethereumAddress = accounts[0];
    });

  }
  ngOnInit() {
  }


  /**
   * Sign Up
   */

  signup() {
    const redirect = `${this._translateService.currentLang}/account/register-done`
    this._http.post('http://localhost:5000/api/v1/users/sign-up', this.model)
      .subscribe((res:any)=> {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            username: res.data.attributes.username,
            ethereumAddress: res.data.attributes.ethereumAddress
          },
          fragment: 'anchor'
        };
        this._router.navigate([redirect], navigationExtras );
        },
        err => {
          console.error(err.error.errors)
        }
      );
  }
}

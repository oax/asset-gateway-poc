const {bytes32, address, create} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER, OPERATOR) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        GateRoles,
        DSGuard,
        FiatToken,
        Gate
    } = contractRegistry

    const gateRoles = await deploy(GateRoles)
    const fiatTokenGuard = await deploy(DSGuard)
    const token = await deploy(FiatToken, address(fiatTokenGuard), web3.utils.utf8ToHex('TOKUSD'))
    const gate = await deploy(Gate, address(gateRoles), address(token))

    const allowGate = ([src, dst, method]) => {
        const methodSig = web3.eth.abi.encodeFunctionSignature(method)
        return fiatTokenGuard.methods.permit(
            bytes32(src),
            bytes32(dst),
            methodSig
        ).send()
    }

    const OPERATOR_ROLE = await gateRoles.methods.OPERATOR().call()

    const allowOperator = method => {
        const methodSig = web3.eth.abi.encodeFunctionSignature(method)
        return gateRoles.methods
            .setRoleCapability(OPERATOR_ROLE, address(gate), methodSig, true)
            .send()
    }

    const methodsAllowedForOperator = [
        'mint(uint256)',
        'mint(address,uint256)',
        'burn(uint256)',
        'burn(address,uint256)'
    ]

    const methodsAllowedForGate = [
        [address(gate), address(token), 'mint(uint256)'],
        [address(gate), address(token), 'mint(address,uint256)'],
        [address(gate), address(token), 'burn(uint256)'],
        [address(gate), address(token), 'burn(address,uint256)']
    ]

    await Promise.all([
        gateRoles.methods
            .setUserRole(OPERATOR, OPERATOR_ROLE, true)
            .send(),
        ...methodsAllowedForOperator.map(allowOperator),
        ...methodsAllowedForGate.map(allowGate)
    ])

    return {gate, token, gateRoles, fiatTokenGuard}
}

module.exports = {
    base
}

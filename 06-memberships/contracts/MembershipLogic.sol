pragma solidity ^0.4.19;


import "MembershipState.sol";
import "Ownable.sol";


contract MembershipLogic is Ownable {
    MembershipState public membershipState;

    mapping (address => bool) public superAdmin;

    mapping (address => bool) public operator;

    function MembershipLogic(MembershipState membershipState_)
    Ownable()
    public
    {
        require(address(membershipState_) != address(0));
        membershipState = membershipState_;
        setSuperAdmin(msg.sender);
    }

    function addMember(uint8 membershipType)
    public
    returns (bool)
    {
        require(membershipType > 0);
        return addMemberFor(membershipType, msg.sender);
    }

    function removeMember(uint8 membershipType)
    public
    returns (bool)
    {
        require(membershipType > 0);
        return removeMemberFor(membershipType, msg.sender);
    }

    function transferMember(uint8 membershipType, address to)
    public
    {
        require(membershipType > 0);
        require(to != address(0));
        transferMemberFor(membershipType, msg.sender, to);
    }


    function addMemberFor(uint8 membershipType, address member)
    public
    returns (bool)
    {
        require(membershipType > 0);
        require(member != address(0));
        return membershipState.addMemberFor(membershipType, msg.sender, member);
    }

    function removeMemberFor(uint8 membershipType, address member)
    public
    returns (bool)
    {
        require(membershipType > 0);
        require(member != address(0));
        require(
        (msg.sender == member)
        || isOperatorOrMoreSuperior(msg.sender)
        );
        return membershipState.removeMemberFor(membershipType, msg.sender, member);
    }

    function transferMemberFor(uint8 membershipType, address from, address to)
    public
    {
        require(
        (membershipType > 0)
        && (from != address(0))
        && (to != address(0))
        );

        require(
        (msg.sender == from)
        || isOperatorOrMoreSuperior(msg.sender)
        );
        membershipState.transferMemberFor(membershipType, from, to);
    }

    function setSuperAdmin(address address_)
    public
    onlyIfSuperAdminOrMoreSuperior
    {
        require(address_ != address(0));
        superAdmin[address_] = true;
    }

    function revokeSuperAdmin(address address_)
    public
    onlyIfSuperAdminOrMoreSuperior
    {
        require(address_ != address(0));
        //don't revoke oneself to avoid human mistakes
        require(address_ != msg.sender);
        superAdmin[address_] = false;
    }

    function setOperator(address address_)
    public
    onlyIfSuperAdminOrMoreSuperior
    {
        require(address_ != address(0));
        operator[address_] = true;
    }

    function revokeOperator(address address_)
    public
    onlyIfSuperAdminOrMoreSuperior
    {
        require(address_ != address(0));
        operator[address_] = false;
    }

    function addMembershipType(uint8 membershipType, uint8 price)
    public
    onlyIfOperatorOrMoreSuperior
    {
        require(membershipType > 0);
        require(price > 0);
        membershipState.addMembershipType(membershipType, price);
    }

    function changeStateOwner(address newOwner)
    public
    onlyIfSuperAdminOrMoreSuperior
    {
        require(newOwner != address(0));
        membershipState.setOwner(newOwner);
    }

    function approveMembership(address address_, uint8 membershipType)
    public
    onlyIfOperatorOrMoreSuperior
    {
        require(address_ != address(0));
        require(membershipType > 0);
        membershipState.approveMembership(address_, membershipType);
    }

    function disapproveMembership(address address_, uint8 membershipType)
    public
    onlyIfOperatorOrMoreSuperior
    {
        require(address_ != address(0));
        require(membershipType > 0);
        membershipState.disapproveMembership(address_, membershipType);
    }

    function isMembershipActive(address address_, uint8 membershipType)
    public
    view
    returns (bool)
    {
        return (
        (address_ != address(0))
        && (membershipType > 0)
        && (membershipState.approved(membershipType, address_) == true)
        && (membershipState.members(membershipType, address_) > 0)
        );
    }

    //used to move accidentally received ERC20 tokens to somewhere manage-able
    function moveAllToken(ERC20Interface token, address to)
    public
    onlyIfSuperAdminOrMoreSuperior
    returns (bool)
    {
        require(address(token) != address(0));
        require(to != address(0));

        uint tokenBalanceInLogicContract = token.balanceOf(address(this));
        bool transferResultForTokenInLogicContract;
        if (tokenBalanceInLogicContract > 0) {
            transferResultForTokenInLogicContract = token.transfer(to, tokenBalanceInLogicContract);
        }

        bool transferResultForTokenInStateContract = membershipState.moveAllToken(token, to);

        return transferResultForTokenInLogicContract && transferResultForTokenInStateContract;
    }

    function setStateWallet(address wallet_)
    onlyIfSuperAdminOrMoreSuperior
    public
    {
        require(wallet_ != address(0));
        membershipState.setWallet(wallet_);
    }

    modifier onlyIfSuperAdminOrMoreSuperior()
    {
        require((isSuperAdmin(msg.sender))
        || isOwner(msg.sender)
        );
        _;
    }

    modifier onlyIfOperatorOrMoreSuperior()
    {
        require(isOperatorOrMoreSuperior(msg.sender));
        _;
    }

    function isOperatorOrMoreSuperior(address address_)
    public
    view
    returns (bool)
    {
        return isOperator(address_)
        || isSuperAdmin(address_)
        || isOwner(address_);
    }

    function isSuperAdmin(address address_)
    public
    view
    returns (bool)
    {
        return (superAdmin[address_] == true);
    }

    function isOperator(address address_)
    public
    view
    returns (bool)
    {
        return (operator[address_] == true);
    }
}
pragma solidity ^0.4.19;


contract Ownable {
    address public owner;

    event LogSetOwner (address indexed owner);

    function Ownable()
    public
    {
        owner = msg.sender;
    }

    modifier onlyIfOwner {
        require(msg.sender == owner);
        _;
    }

    function setOwner(address owner_)
    public
    onlyIfOwner
    {
        owner = owner_;
        LogSetOwner(owner);
    }

    function isOwner(address address_)
    public
    view
    returns (bool)
    {
        return address_ == owner;
    }
}

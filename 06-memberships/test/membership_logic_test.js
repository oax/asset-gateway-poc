const {
    expect,
    expectAsyncThrow,
    expectNoAsyncThrow,
    expectThrow,
    toBN,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')

const {
    address,
    wad,
    send,
    call,
    txEvents,
    sendEth
} = require('chain-dsl')

const deployer = require('../lib/deployer')

const addMembershipType = 'addMembershipType(uint8,uint8)'
const addMember = 'addMember(uint8)'
const addMemberFor = 'addMemberFor(uint8,address)'
const transferMember = 'transferMember(uint8,address)'
const transferMemberFor = 'transferMemberFor(uint8,address,address)'
const removeMember = 'removeMember(uint8)'
const removeMemberFor = 'removeMemberFor(uint8,address)'
const members = 'members(uint8,address)'
const approve = 'approve'
const lastLimitResetTime = 'lastLimitResetTime'
const PARTICIPANT_MEMBERSHIP_NAME = 1
const VOTING_MEMBERSHIP_NAME = 2
const NEW_MEMBERSHIP_NAME = 3

describe("OAX Foundation Membership Logic Contract", function () {
    let web3, snaps, accounts,
        oaxToken,
        membershipState,
        memberships,
        randomErc20Token,
        DEPLOYER,
        OPERATOR,
        CUSTOMER,
        CUSTOMER_TWO,
        MIN_AMT,
        AMT,
        DEFAULT_DAILY_LIMIT,
        PARTICIPANT_MEMBERSHIP_FEE,
        VOTING_MEMBERSHIP_FEE

    before('deployment', async () => {
        this.timeout(6000)
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER,
            CUSTOMER_TWO
        ] = accounts = await web3.eth.getAccounts()

        MIN_AMT = wad(1)
        AMT = wad(100)
        DEFAULT_DAILY_LIMIT = wad(10000)
        PARTICIPANT_MEMBERSHIP_FEE = AMT
        VOTING_MEMBERSHIP_FEE = AMT * 2

        ;({
            oaxToken,
            membershipState,
            memberships,
            randomErc20Token
        } = await deployer.base(web3, solc(__dirname, '../solc-input.json'),
            DEPLOYER,
            OPERATOR,
            OPERATOR,
            DEFAULT_DAILY_LIMIT))

        //set state contract owner to be logic contract
        await send(membershipState, DEPLOYER, "setOwner", memberships.options.address)
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    context("Add Member", function () {
        beforeEach("OAX Token Sale contract owner marks operator and customer kyc verified;" +
            "and transfers customer enough oax token.", async () => {
            await send(memberships, DEPLOYER, "setOperator", OPERATOR)

            await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, VOTING_MEMBERSHIP_FEE)
            await send(oaxToken, DEPLOYER, "kycVerify", OPERATOR)
            await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER)

            await send(oaxToken, DEPLOYER, "finalise")
            await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, VOTING_MEMBERSHIP_FEE)
        })

        it("One customer should successfully add oneself as Participant Member and transfer membership fee to Membership Wallet if everything is in line.", async () => {
            let membershipWallet = await call(membershipState, "wallet")
            let beforeWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)

            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            let afterWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            expect(membershipCount > 0).eql(true)
            expect(afterWalletBalance - beforeWalletBalance).eq(PARTICIPANT_MEMBERSHIP_FEE)
        })

        it("One customer should fail to add oneself as Participant Member if OAX Token allowance is not enough.", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
                let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                expect(membershipCount > 0).eql(true)
            })
        })

        //FIXME mimic transfer fail cases like out of gas etc.
        it.skip("SKIPPED: One customer should fail to add oneself as Participant Member if OAX Token allowance is enough but transfer fails.", async () => {
            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
            // await send(oaxToken, OPERATOR, "transfer", CUSTOMER, PARTICIPANT_MEMBERSHIP_FEE)

            await expectAsyncThrow(async () => {
                await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
                let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                expect(membershipCount > 0).eql(true)
            })
        })

        it("One customer should successfully add oneself as Voting Member and transfer membership fee to Membership Wallet.", async () => {
            let membershipWallet = await call(membershipState, "wallet")
            let beforeWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            await send(oaxToken, CUSTOMER, approve, address(membershipState), VOTING_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, VOTING_MEMBERSHIP_NAME)

            let membershipCount = await call(membershipState, members, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            let afterWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            expect(membershipCount > 0).eql(true)
            expect(afterWalletBalance - beforeWalletBalance).eq(VOTING_MEMBERSHIP_FEE)
        })


        it("One customer should fail to add non-existing membership.", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, CUSTOMER, addMember, NEW_MEMBERSHIP_NAME)
            })
        })

        it("One customer can apply for Participant Membership for another customer.", async () => {
            let membershipWallet = await call(membershipState, "wallet")
            let beforeWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMemberFor, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)

            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            let afterWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            expect(membershipCount > 0).eql(true)
            expect(afterWalletBalance - beforeWalletBalance).eq(PARTICIPANT_MEMBERSHIP_FEE)
        })

        it("One customer can have multiple Participant Memberships.", async () => {
            let membershipWallet = await call(membershipState, "wallet")
            let beforeWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            await send(oaxToken, CUSTOMER, approve, address(membershipState), VOTING_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, VOTING_MEMBERSHIP_NAME)

            let membershipCount = await call(membershipState, members, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            let afterWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            expect(membershipCount > 0).eql(true)
            expect(afterWalletBalance - beforeWalletBalance).eq(VOTING_MEMBERSHIP_FEE)
            expect(membershipCount).eq(1)

            //make sure customer has enough money
            await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, VOTING_MEMBERSHIP_FEE)

            beforeWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            await send(oaxToken, CUSTOMER, approve, address(membershipState), VOTING_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, VOTING_MEMBERSHIP_NAME)

            membershipCount = await call(membershipState, members, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            afterWalletBalance = await call(oaxToken, "balanceOf", membershipWallet)

            expect(membershipCount > 0).eql(true)
            expect(afterWalletBalance - beforeWalletBalance).eq(VOTING_MEMBERSHIP_FEE)
            expect(membershipCount).eq(2)
        })

        context("Requester is always the one paying for fees when adding members.", async () => {
            beforeEach("Make sure OPERATOR has enough OAX tokens.", async () => {
                await send(oaxToken, DEPLOYER, "transfer", OPERATOR, PARTICIPANT_MEMBERSHIP_FEE)
            })

            it("OPERATOR should fail to request Participant Membership for customer if OPERATOR doesn't approve spending OPERATOR's OAX.", async () => {
                await expectAsyncThrow(async () => {
                    await send(memberships, OPERATOR, addMemberFor, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                })
            })

            it("OPERATOR should succeed in requesting Participant Membership for customer if OPERATOR approves spending OPERATOR's OAX.", async () => {
                await send(oaxToken, OPERATOR, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
                await expectNoAsyncThrow(async () => {
                    await send(memberships, OPERATOR, addMemberFor, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                })
            })
        })
    })

    context("Transfer Membership", function () {
        beforeEach("OAX Token Sale contract owner marks operator and customer kyc verified;" +
            "and transfers customer enough oax token.", async () => {
            await send(memberships, DEPLOYER, "setOperator", OPERATOR)

            await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, VOTING_MEMBERSHIP_FEE)
            await send(oaxToken, DEPLOYER, "kycVerify", OPERATOR)
            await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER)

            await send(oaxToken, DEPLOYER, "finalise")
            await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, VOTING_MEMBERSHIP_FEE)
        })

        it("One customer should successfully transfer Participant Membership from oneself to customer_two.", async () => {
            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
            await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER_TWO)
            await send(memberships, CUSTOMER, transferMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            expect(membershipCount > 0).eql(true)
        })

        it("One customer should successfully transfer Participant Memberships from oneself to customer_two twice.", async () => {
            //first, make sure have enough OAX to pay for fee
            await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, PARTICIPANT_MEMBERSHIP_FEE)

            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE * 2)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
            await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER_TWO)
            await send(memberships, CUSTOMER, transferMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            await send(memberships, CUSTOMER, transferMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            expect(membershipCount > 0).eql(true)
            expect(membershipCount).eq(2)
            expect(await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)).eq(0)
        })

        it("One customer should successfully transfer Participant Memberships from oneself to customer_two and operator.", async () => {
            //first, make sure have enough OAX to pay for fee
            await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, PARTICIPANT_MEMBERSHIP_FEE)

            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE * 2)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
            await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER_TWO)

            await send(memberships, CUSTOMER, transferMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            await send(memberships, CUSTOMER, transferMember, PARTICIPANT_MEMBERSHIP_NAME, OPERATOR)
            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            expect(membershipCount > 0).eql(true)
            expect(membershipCount).eq(1)
            expect(await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, OPERATOR)).eq(1)
            expect(await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)).eq(0)
        })

        it("One customer should fail to transfer Participant Membership from oneself to customer_two if the customer has no such membership.", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, CUSTOMER, transferMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            })
            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            expect(membershipCount > 0).eql(false)
            membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            expect(membershipCount > 0).eql(false)
        })

        context("Requester must be the one owning the membership or an OAX Foundation authorised address.", async () => {
            beforeEach(
                "Add Participant Membership for customer; " +
                "Add Participant Membership for operator; " +
                "Authorise operator by the membership contract operator admin.",
                async () => {
                    await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
                    await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
                    await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER_TWO)
                    await send(memberships, DEPLOYER, "setOperator", OPERATOR)
                }
            )

            it("Customer two, which is not a OAX Foundation authorised address, should fail to transfer Participant Membership owned by any other address.",
                async () => {
                    await expectAsyncThrow(async () => {
                        await send(memberships, CUSTOMER_TWO, transferMemberFor, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER, OPERATOR)
                    })
                }
            )

            context("OAX Foundation authorised address should successfully transfer any address's Participant Membership.", async () => {
                it("Any OAX Foundation authorised address should successfully transfer Participant Membership owned by any other address.", async () => {
                    await expectNoAsyncThrow(async () => {
                        await send(memberships, OPERATOR, transferMemberFor, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER, OPERATOR)
                    })
                })
            })
        })

        //not testable because token sale contracts only enable kyc check for address that participant in token sale period,
        // this is only testable until we decide to implement our own set of kyc mapping on the blockchain
        it.skip("SKIPPED: One customer should fail to transfer Participant Membership from oneself to customer_two if customer_two is not kyc verified.", async () => {
            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
            await expectAsyncThrow(async () => {
                await send(memberships, CUSTOMER, transferMember, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            })
            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER_TWO)
            expect(membershipCount > 0).eql(false)
        })
    })


    context("Initial Config", function () {
        it("OAX Membership Contract can call OAX Token Sale Contract.", async () => {
            let oaxAllowance = await call(membershipState, "oaxAllowance", CUSTOMER, CUSTOMER_TWO)
            expect(oaxAllowance).eq(0)
        })

        it("OAX Membership Contract can query legit OAX Token allowance figures.", async () => {
            await send(oaxToken, OPERATOR, approve, CUSTOMER, PARTICIPANT_MEMBERSHIP_FEE)
            let oaxAllowance = await call(membershipState, "oaxAllowance", OPERATOR, CUSTOMER)
            expect(oaxAllowance).eq(PARTICIPANT_MEMBERSHIP_FEE)
            oaxAllowance = await call(membershipState, "oaxAllowance", CUSTOMER, OPERATOR)
            expect(oaxAllowance).eq(0)
        })

        it("Membership Wallet is initiated when OAX Membership Contract is deployed.", async () => {
            let membershipWallet = await call(membershipState, "wallet")
            expect(membershipWallet).to.exist
        })
    })

    context("Remove Member", function () {
        beforeEach("OAX Token Sale contract owner marks operator and customer kyc verified;" +
            "and transfers customer enough oax token.", async () => {
            await send(memberships, DEPLOYER, "setOperator", OPERATOR)
            await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, VOTING_MEMBERSHIP_FEE)
            await send(oaxToken, DEPLOYER, "kycVerify", OPERATOR)
            await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER)

            await send(oaxToken, DEPLOYER, "finalise")
            await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, VOTING_MEMBERSHIP_FEE)
        })

        it("One customer should successfully remove oneself as Participant Member.", async () => {
            await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
            let membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            expect(membershipCount > 0).eql(true)
            await send(memberships, CUSTOMER, removeMember, PARTICIPANT_MEMBERSHIP_NAME)
            membershipCount = await call(membershipState, members, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
            expect(membershipCount > 0).eql(false)
        })

        it("One customer should successfully remove oneself as Voting Member.", async () => {
            await send(oaxToken, CUSTOMER, approve, address(membershipState), VOTING_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, VOTING_MEMBERSHIP_NAME)
            let membershipCount = await call(membershipState, members, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            expect(membershipCount > 0).eql(true)
            await send(memberships, CUSTOMER, removeMember, VOTING_MEMBERSHIP_NAME)
            membershipCount = await call(membershipState, members, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            expect(membershipCount > 0).eql(false)
        })

        it("One customer should fail to remove oneself as Voting Member twice if the customer only has one Voting Membership.", async () => {
            await send(oaxToken, CUSTOMER, approve, address(membershipState), VOTING_MEMBERSHIP_FEE)
            await send(memberships, CUSTOMER, addMember, VOTING_MEMBERSHIP_NAME)
            let membershipCount = await call(membershipState, members, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            expect(membershipCount > 0).eql(true)
            await send(memberships, CUSTOMER, removeMember, VOTING_MEMBERSHIP_NAME)
            await expectAsyncThrow(async () => {
                await send(memberships, CUSTOMER, removeMember, VOTING_MEMBERSHIP_NAME)
            })
            membershipCount = await call(membershipState, members, VOTING_MEMBERSHIP_NAME, CUSTOMER)
            expect(membershipCount > 0).eql(false)
        })

        context("Requester must be the one owning the membership or an OAX Foundation authorised address.", async () => {
            beforeEach(
                "Add Participant Membership for customer; " +
                "Add Participant Membership for operator; " +
                "Authorise operator by the membership contract operator admin.",
                async () => {
                    await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
                    await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
                    await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER_TWO)
                    await send(memberships, DEPLOYER, "setOperator", OPERATOR)
                }
            )

            it("Customer two, which is not a OAX Foundation authorised address, should fail to transfer Participant Membership owned by any other address.",
                async () => {
                    await expectAsyncThrow(async () => {
                        await send(memberships, CUSTOMER_TWO, removeMemberFor, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                    })
                }
            )

            context("OAX Foundation authorised address should successfully transfer any address's Participant Membership.", async () => {
                it("Any OAX Foundation authorised address should successfully transfer Participant Membership owned by any other address.", async () => {
                    await expectNoAsyncThrow(async () => {
                        await send(memberships, OPERATOR, removeMemberFor, PARTICIPANT_MEMBERSHIP_NAME, CUSTOMER)
                    })
                })
            })
        })
    })

    context("Authorisation", function () {
        it("Only authorised super admin addresses can add or revoke other super admin addresses.", async () => {
            await expectNoAsyncThrow(async () => {
                await send(memberships, DEPLOYER, "setSuperAdmin", OPERATOR)
                expect(await call(memberships, "superAdmin", OPERATOR)).eql(true)

                await send(memberships, DEPLOYER, "revokeSuperAdmin", OPERATOR)
                expect(await call(memberships, "superAdmin", OPERATOR)).eql(false)
            })
        })

        it("Non super admin addresses can NOT add any super admin.", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, "setSuperAdmin", DEPLOYER)
            })
        })

        it("Non super admin addresses can NOT revoke any super admin.", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, "revokeSuperAdmin", DEPLOYER)
            })
        })

        it("Authorised super admin addresses can NOT revoke oneself as super admin to avoid human mistakes.", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, DEPLOYER, "revokeSuperAdmin", DEPLOYER)
            })
        })

        it("Only authorised super admin addresses can add or revoke operator addresses.", async () => {
            await send(memberships, DEPLOYER, "setOperator", OPERATOR)
            expect(await call(memberships, "operator", OPERATOR)).eql(true)

            await send(memberships, DEPLOYER, "revokeOperator", OPERATOR)
            expect(await call(memberships, "operator", OPERATOR)).eql(false)

            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, "setOperator", DEPLOYER)
            })
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, "revokeOperator", DEPLOYER)
            })
        })
    })

    context("Extensibility", function () {

        it("Only authorised (owner, super admin or operator) addresses can add membership types.", async () => {
            await expectAsyncThrow(async () => {
                await txEvents(send(memberships, OPERATOR, "addMembershipType", 10, 100))
            })

            await expectNoAsyncThrow(async () => {
                await txEvents(send(memberships, DEPLOYER, "addMembershipType", 10, 100))
            })

            await expectNoAsyncThrow(async () => {
                await send(memberships, DEPLOYER, "setOperator", OPERATOR)
                await txEvents(send(memberships, OPERATOR, "addMembershipType", 11, 110))
            })
        })

        it("Existing memberships can be updated with positive price.", async () => {
            await send(memberships, DEPLOYER, "setOperator", OPERATOR)

            await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
            await expectAsyncThrow(async () => {
                await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, 0)
            })
            await expectNoAsyncThrow(async () => {
                await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, VOTING_MEMBERSHIP_FEE)
            })
        })

        it("Different memberships can have same price.", async () => {
            await expectNoAsyncThrow(async () => {
                await send(memberships, DEPLOYER, "setOperator", OPERATOR)
                await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, AMT)
                await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, AMT)
            })
        })

        it.skip("SKIPPED: Already added membership types cannot be removed by anyone since it is already in use.", async () => {
            await expectAsyncThrow(async () => {
                //-10 would be interpreted as 246, so this test is not testable
                await (send(memberships, OPERATOR, addMembershipType, -10, -10))
            })
        })
    })

    context("Membership Approval", function () {
        context("Only authorised (owner, super admin or operator) addresses can approve each address's each membership separately.", async () => {
            beforeEach(
                "Empower customer_two as super admin;" +
                "empower operator as operator;" +
                "a customer adds participant membership to itself by paying fee;" +
                "s/he also gets voting membership because customer_two transferred the voting membership to him/her.", async () => {
                    await send(memberships, DEPLOYER, "setSuperAdmin", CUSTOMER_TWO)
                    await send(memberships, DEPLOYER, "setOperator", OPERATOR)

                    await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, PARTICIPANT_MEMBERSHIP_FEE)
                    await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, VOTING_MEMBERSHIP_FEE)
                })

            it("Super admin can approve the customer for its participant membership.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, CUSTOMER_TWO, "approveMembership", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)
                })
            })

            it("Super admin can disapprove the customer for its participant membership after it's approved.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, CUSTOMER_TWO, "disapproveMembership", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)
                })
            })

            it("Super admin can approve the customer for its voting membership.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, CUSTOMER_TWO, "approveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                })
            })

            it("Super admin can disapprove the customer for its voting membership after it's approved.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, CUSTOMER_TWO, "disapproveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                })
            })

            it("Operator can approve the customer for its participant membership.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, OPERATOR, "approveMembership", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)
                })
            })

            it("Operator can disapprove the customer for its participant membership after it's approved.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, OPERATOR, "disapproveMembership", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)
                })
            })

            it("Operator can approve the customer for its voting membership.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, OPERATOR, "approveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                })
            })

            it("Operator can disapprove the customer for its voting membership after it's approved.", async () => {
                await expectNoAsyncThrow(async () => {
                    await send(memberships, OPERATOR, "disapproveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                })
            })

            it("Unauthorised addresses could NEVER approve the customer for its participant membership.", async () => {
                await expectAsyncThrow(async () => {
                    await send(memberships, CUSTOMER, "approveMembership", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)
                })
            })

            it("Unauthorised addresses could NEVER disapprove the customer for its participant membership after it's approved.", async () => {
                await send(memberships, OPERATOR, "approveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                await expectAsyncThrow(async () => {
                    await send(memberships, CUSTOMER, "disapproveMembership", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)
                })
            })

            it("Unauthorised addresses could NEVER approve the customer for its voting membership.", async () => {
                await expectAsyncThrow(async () => {
                    await send(memberships, CUSTOMER, "approveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                })
            })

            it("Unauthorised addresses could NEVER disapprove the customer for its voting membership after it's approved.", async () => {
                await send(memberships, OPERATOR, "approveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                await expectAsyncThrow(async () => {
                    await send(memberships, CUSTOMER, "disapproveMembership", CUSTOMER, VOTING_MEMBERSHIP_NAME)
                })
            })
        })

        context("An address's membership is considered active only if it has the membership ticket (by paying fee or get transferred) and it is approved for this membership.", async () => {
            beforeEach(
                "Empower customer_two as super admin;" +
                "empower operator as operator;" +
                "a customer adds participant membership to itself by paying fee;" +
                "s/he also gets voting membership because customer_two transferred the voting membership to him/her.", async () => {
                    await send(memberships, DEPLOYER, "setSuperAdmin", CUSTOMER_TWO)
                    await send(memberships, DEPLOYER, "setOperator", OPERATOR)

                    await send(memberships, OPERATOR, addMembershipType, PARTICIPANT_MEMBERSHIP_NAME, PARTICIPANT_MEMBERSHIP_FEE)
                    await send(memberships, OPERATOR, addMembershipType, VOTING_MEMBERSHIP_NAME, VOTING_MEMBERSHIP_FEE)

                    await send(oaxToken, DEPLOYER, "kycVerify", OPERATOR)
                    await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER)
                    await send(oaxToken, DEPLOYER, "kycVerify", CUSTOMER_TWO)
                    await send(oaxToken, DEPLOYER, "finalise")
                    await send(oaxToken, DEPLOYER, "transfer", CUSTOMER_TWO, VOTING_MEMBERSHIP_FEE)
                    await send(oaxToken, DEPLOYER, "transfer", CUSTOMER, PARTICIPANT_MEMBERSHIP_FEE)
                    await send(oaxToken, CUSTOMER_TWO, approve, address(membershipState), VOTING_MEMBERSHIP_FEE)
                    await send(memberships, CUSTOMER_TWO, addMember, VOTING_MEMBERSHIP_NAME)

                    await send(oaxToken, CUSTOMER, approve, address(membershipState), PARTICIPANT_MEMBERSHIP_FEE)
                    await send(memberships, CUSTOMER, addMember, PARTICIPANT_MEMBERSHIP_NAME)
                    await send(memberships, CUSTOMER_TWO, transferMember, VOTING_MEMBERSHIP_NAME, CUSTOMER_TWO)
                })

            it("Customer's participant membership is not active when it pays it's fee but not approved.", async () => {
                expect(await call(memberships, "isMembershipActive", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)).eql(false)
            })
            it("Customer's participant membership is active when it pays it's fee and gets approved.", async () => {
                await send(memberships, OPERATOR, "approveMembership", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)
                expect(await call(memberships, "isMembershipActive", CUSTOMER, PARTICIPANT_MEMBERSHIP_NAME)).eql(true)
            })
            it("Customer_two's voting membership is not active when it is approved but has got no ticket.", async () => {
                expect(await call(memberships, "isMembershipActive", CUSTOMER_TWO, VOTING_MEMBERSHIP_NAME)).eql(false)
            })
        })
    })

    //higher priority
    //TODO integrate test against already deployed token sale contract rather than some contract only in memory
    //TODO edge cases like allowance approval success but transfer failure
    //TODO separate roles states into another state contract?
    //TODO make the contracts finalize-able/pausable-able (in case of any emergency)

    //lower priority
    //TODO batch approval

    //don't be greedy and don't lock tokens! reference: https://arxiv.org/abs/1802.06038
    context("Return other ERC20 tokens accidentally received", async () => {
        beforeEach("Logic contract accidentally gets some random ERC20 token; " +
            "state contract accidentally gets some random ERC20 token.", async () => {
            await send(randomErc20Token, DEPLOYER, 'transfer', memberships.options.address, 1000)
            await send(randomErc20Token, DEPLOYER, 'transfer', membershipState.options.address, 100)
        })

        it("Random ERC20 Tokens in logic and state contracts are both transferred to wallet", async () => {
            await send(memberships, DEPLOYER, "moveAllToken", randomErc20Token.options.address, CUSTOMER)
            expect(await call(randomErc20Token, "balanceOf", CUSTOMER)).eq(1100)
        })
    })

    context("State wallet address can be changed by owner and super admin only.", async () => {
        it("Owner can change state wallet.", async () => {
            await expectNoAsyncThrow(async () => {
                await send(memberships, DEPLOYER, "setStateWallet", CUSTOMER)
            })
            expect(await call(membershipState, "wallet")).eq(CUSTOMER)
        })

        it("Customer can NOT change state wallet.", async () => {
            await expectAsyncThrow(async () => {
                await send(memberships, CUSTOMER, "setStateWallet", CUSTOMER)
            })
            expect(await call(membershipState, "wallet")).eq(OPERATOR)
        })
    })
})

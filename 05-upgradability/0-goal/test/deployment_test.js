const {
    expect,
    solc,
    ganacheWeb3,
} = require('chain-dsl/test/helpers')

const deploy = require('../lib/deployer')
const {call} = require('chain-dsl')

describe('Deployment', function () {
    let web3, snaps, accounts, DEPLOYER, OPERATOR, CUSTOMER, token

    before('deployment', async () => {
        snaps = []
        web3 = ganacheWeb3()
        ;[
            DEPLOYER,
            OPERATOR,
            CUSTOMER
        ] = accounts = await web3.eth.getAccounts()

        ;({token} = await deploy.base(web3, solc(__dirname, '../solc-input.json'), DEPLOYER, OPERATOR))
    })

    beforeEach(async () => snaps.push(await web3.evm.snapshot()))
    afterEach(async () => web3.evm.revert(snaps.pop()))

    it('Token is deployed', async () => {
        const supply = await call(token, 'totalSupply')
        expect(supply).eq(1000)
    })

})

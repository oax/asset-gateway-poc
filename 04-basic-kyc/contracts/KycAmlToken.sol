pragma solidity ^0.4.19;


import 'ds/erc20.sol';
import 'KycAmlRule.sol';


contract KycAmlToken {
    KycAmlRule public kycAmlRule;

    mapping(address => bool) public kycVerified;

    function KycAmlToken(KycAmlRule kycAmlRule_) public {
        kycAmlRule = kycAmlRule_;
    }

    // Checks whether it can transfer or otherwise throws.
    modifier checkTransfer(address _from, address _to, uint _value) {
        require(kycAmlRule.canTransfer(address(this), _from, _to, _value));
        _;
    }

    modifier checkDeposit(uint _value){
        require(kycAmlRule.canDeposit(address(this), msg.sender, _value));
        _;
    }

    modifier checkDepositTo(address _to, uint _value){
        require(kycAmlRule.canDeposit(address(this), _to, _value));
        _;
    }

    modifier checkWithdraw(uint _value){
        require(kycAmlRule.canWithdraw(address(this), msg.sender, _value));
        _;
    }

    modifier checkWithdrawFrom(address _from, uint _value){
        require(kycAmlRule.canWithdraw(address(this), _from, _value));
        _;
    }

    function isKycVerified(address guy) public view returns (bool) {
        return kycVerified[guy];
    }
}

const {create} = require('chain-dsl')

const base = async (web3, contractRegistry, DEPLOYER, OPERATOR) => {
    const deploy = (...args) => create(web3, DEPLOYER, ...args)

    const {
        Gate,
        AccessControl,
        NoKycAmlRule,
        BoundaryKycAmlRule,
        FullKycAmlRule
    } = contractRegistry

    const accessControl = await deploy(AccessControl)
    const noKycAmlRule = await deploy(NoKycAmlRule)
    const boundaryKycAmlRule = await deploy(BoundaryKycAmlRule)
    const fullKycAmlRule = await deploy(FullKycAmlRule)
    const gate = await deploy(Gate, accessControl.options.address, noKycAmlRule.options.address)
    const boundaryKycGate = await deploy(Gate, accessControl.options.address, boundaryKycAmlRule.options.address)
    const fullKycGate = await deploy(Gate, accessControl.options.address, fullKycAmlRule.options.address)
    const OPERATOR_ROLE = await accessControl.methods.OPERATOR().call()

    const allow = method => {
        const methodSig = web3.eth.abi.encodeFunctionSignature(method)
        accessControl.methods
            .setRoleCapability(OPERATOR_ROLE, gate.options.address, methodSig, true)
            .send()
        accessControl.methods
            .setRoleCapability(OPERATOR_ROLE, boundaryKycGate.options.address, methodSig, true)
            .send()
        return accessControl.methods
            .setRoleCapability(OPERATOR_ROLE, fullKycGate.options.address, methodSig, true)
            .send()
    }

    const methodsAllowedForOperator = [
        'balanceOf(address)',
        'mint(uint256)',
        'mint(address,uint256)',
        'mintFor(address,uint256)',
        'burn(address,uint256)',
        'burn(uint256)',
        'burnFrom(address,uint256)',
        'setKycVerified(address,bool)'
    ]

    await Promise.all([
        accessControl.methods
            .setUserRole(OPERATOR, OPERATOR_ROLE, true)
            .send(),
        ...methodsAllowedForOperator.map(allow)
    ])

    return {
        gate,
        boundaryKycGate,
        fullKycGate,
        accessControl
    }
}

module.exports = {
    base
}

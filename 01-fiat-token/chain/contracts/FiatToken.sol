pragma solidity ^0.4.17;

import 'zeppelin-solidity/contracts/token/MintableToken.sol';
import 'zeppelin-solidity/contracts/token/BurnableToken.sol';

contract FiatToken is MintableToken, BurnableToken {
    function FiatToken() public {
    }

    function id() public pure returns(uint) {
        return 42;
    }
}
